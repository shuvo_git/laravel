<?php

use Illuminate\Database\Seeder;
use App\Film;

class FilmsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
 
        // Create 20 product records
        for ($i = 0; $i < 20; $i++) {
            Film::create([
                'name' => $faker->title,
                'rating' => $faker->randomNumber(1),
                'price' => $faker->randomNumber(2),
                'release_date'=>$faker->date(),
                'ticket'=>$faker->randomNumber(3),
                'genre'=>json_encode(["romantic","action"]),
                'photo'=>$faker->imageUrl
            ]);
        }
    }
}
