<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('films', function (Blueprint $table) 
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('name',50);
            $table->date('release_date');
            $table->decimal('rating', 3, 2);
            $table->integer('ticket');
            $table->integer('price');
            $table->json('genre');
            $table->string('photo',300);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('films');
    }
}
