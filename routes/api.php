<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Film;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
| https://code.tutsplus.com/tutorials/build-a-react-app-with-laravel-restful-backend-part-1-laravel-5-api--cms-29442
|
*/

/**
** Basic Routes for a RESTful service:
**
** Route::get($uri, $callback);
** Route::post($uri, $callback);
** Route::put($uri, $callback);
** Route::delete($uri, $callback);
**
**/

Route::get('films', 'FilmsController@index');
 
Route::get('films/{film}', 'FilmsController@show');
 
Route::post('films','FilmsController@store');
 
Route::put('films/{film}','FilmsController@update');
 
Route::delete('films/{film}', 'FilmsController@delete');

 
Route::get('films', function () {
    return response(Film::all(),200);
});
 
Route::get('films/{film}', function ($filmId) {
    return response(Film::find($filmId), 200);
});
  
 
Route::post('films', function(Request $request) {
    $resp = Film::create($request->all());
    return $resp;
});
 
Route::put('films/{film}', function(Request $request, $filmId) {
    $film = Film::findOrFail($filmId);
    $film->update($request->all());
    return $film;
});
 
Route::delete('films/{film}',function($filmId) {
    Film::find($filmId)->delete();
 
    return 204;
 
});

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/