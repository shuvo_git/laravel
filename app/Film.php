<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    protected $fillable = ['name', 'release_date', 'rating', 'genre','photo'];
    protected $table = "films";
    //
}
