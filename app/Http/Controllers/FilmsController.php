<?php

namespace App\Http\Controllers\FilmsController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\Film;

class FilmsController extends Controller{
    
    public function index()
    {
        return Film::all();
    }
 
    public function show(Film $film)
    {
        return $film;
    }
 
    public function store(Request $request)
    {
        $film = Film::create($request->all());
 
        return response()->json($film, 201);
    }
 
    public function update(Request $request, Film $film)
    {
        $film->update($request->all());
 
        return response()->json($film, 200);
    }
 
    public function delete(Film $film)
    {
        $film->delete();
 
        return response()->json(null, 204);
    }
}

